//
//  filip.cpp
//  filip
//
//  Created by Ryan Fleming on 2/22/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <math.h>

int main(int argc, const char * argv[]) {
    int a, b, c=0, d=0;
    std::cin>>a;
    std::cin>>b;
    int n=2;
    while(a!=0)
    {
        c+=(a%10*pow(10,n));
        a/=10;
        n--;
    }
    n=2;
    while(b!=0)
    {
        d+=(b%10*pow(10,n));
        b/=10;
        n--;
    }
    if(c>d)
    {
        std::cout<<c;
    }
    else{
        std::cout<<d;
    }
    return 0;
}
