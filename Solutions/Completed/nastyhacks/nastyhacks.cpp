//
//  nastyhacks.cpp
//  nastyhacks
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int runs, rna, rwa, coa;
    std::cin>>runs;
    for(int i=0;i<runs;i++)
    {
        std::cin>>rna;
        std::cin>>rwa;
        std::cin>>coa;
        if(rna>(rwa-coa))
        {
            std::cout << "do not advertise" << std::endl;
        }
        if(rna==(rwa-coa))
        {
            std::cout << "does not matter" << std::endl;
        }
        if(rna<(rwa-coa))
        {
            std::cout << "advertise" << std::endl;
        }
    }
    
    return 0;
}
