//
//  zamka.cpp
//  Zamka
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <cassert>

using namespace std;

int min(int a, int b, int c)
{
    int sum=0;
    int temp;
    for(int i=a; i<=b; i++)
    {
        temp=i;
        sum=0;
        while(temp>0)
        {
            sum+=temp%10;
            temp/=10;
        }
        if(sum==c)
        {
            
            return i;
        }
    }
    return 0;
}
int max(int a, int b, int c)
{
    int sum=0;
    int temp;
    for(int i=b; i>=a; i--)
    {
        temp=i;
        sum=0;
        while(temp>0)
        {
            sum+=temp%10;
            temp/=10;
        }
        if(sum==c)
        {
            return i;
        }
    }
    return 0;
}

int main(int argc, const char * argv[]) {
    int a,b,c;
    cin>>a;
    cin>>b;
    cin>>c;
    cout << min(a,b,c) << endl;
    cout << max(a,b,c) << endl;
}
