//
//  oddities.cpp
//  oddities
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <math.h>

int main(int argc, const char * argv[]) {
    int runs,temp;
    std::cin>>runs;
    for(int i=0;i<runs;i++)
    {
        std::cin>>temp;
        if(abs(temp)%2==0)
        {
            std::cout << temp << " is even"<<std::endl;
        }
        else
        {
            std::cout << temp << " is odd"<<std::endl;
        }
    }
    
    return 0;
}
