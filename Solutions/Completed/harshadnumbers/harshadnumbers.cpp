//
//  harshadnumbers.cpp
//  harshadnumbers
//
//  Created by Ryan Fleming on 2/21/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int input, sum, temp, ans, i;
    ans=0;
    bool yikes=true;
    std::cin>>input;
    i=input;
    while(yikes==true)
    {
        
        temp=i;
        sum=0;
        while(temp!=0)
        {
            sum+=temp%10;
            temp/=10;
        }
        if(i%sum==0)
        {
            ans=i;
            yikes=false;
        }
        else
        {
            i++;
        }
    }
    std::cout << ans;
    return 0;
}

