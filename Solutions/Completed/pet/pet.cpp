//
//  pet.cpp
//  pet
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int winningSum,winningPlayer,sum,temp;
    winningPlayer=0;
    winningSum=0;
    for(int i=0; i<5;i++)
    {
        sum=0;
        for(int j=0;j<4;j++)
        {
            std::cin>>temp;
            sum+=temp;
        }
        if(winningSum<sum)
        {
            winningSum=sum;
            winningPlayer=i+1;
        }
    }

    std::cout << winningPlayer << " " << winningSum;
    return 0;
}
