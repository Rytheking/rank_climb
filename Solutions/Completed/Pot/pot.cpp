//
//  pot.cpp
//  Solutions
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <math.h>
#include <cassert>

using namespace std;

int main(int argc, const char * argv[]) {
    int r, sum,tempVal, base, power;
    cin>>r;
    sum=0;
    for(int i=0; i<r; i++)
    {
        cin>>tempVal;
        base=tempVal/10;
        
        power=tempVal%10;
        
        sum+=pow(base, power);
    }
    cout << sum << endl;
    return 0;
}
