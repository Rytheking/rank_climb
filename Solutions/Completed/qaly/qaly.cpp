//
//  qaly.cpp
//  qaly
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int runs;
    double sum, a,b;
    std::cin>>runs;
    sum=0;
    for(int i=0; i<runs;i++)
    {
        std::cin>>a;
        std::cin>>b;
        sum+=(a*b);
    }
    
    std::cout << sum;
    return 0;
}
