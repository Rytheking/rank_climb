//
//  mountainbiking.cpp
//  mountainbiking
//
//  Created by Ryan Fleming on 2/22/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <math.h>

int main(int argc, const char * argv[]) {
    int num_segs;
    double pi=atan(1)*4;
    float gravity,temp,vel;
    std::cin>>num_segs;
    std::cin>>gravity;
    int segs[num_segs][num_segs];
    for(int i=0;i<num_segs;i++)
    {
        std::cin>>segs[i][0];
        std::cin>>segs[i][1];
    }
    for(int i=0;i<num_segs;i++)
    {
        
        vel=0;
        for(int j=i; j<num_segs;j++)
        {
            temp= vel*vel + 2*(gravity*cos(segs[j][1]*pi/180))*segs[j][0];
            vel=sqrt(temp);
        }
        printf("%.7f", vel);
        std::cout<<std::endl;
    }
    return 0;
}
