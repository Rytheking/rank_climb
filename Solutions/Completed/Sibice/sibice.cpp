//
//  sibice.cpp
//  sibice
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <cmath>

using namespace std;
int main(int argc, const char * argv[]) {
    int max, w, h, maxLength, temp;
    cin>>max;
    cin>>w;
    cin>>h;
    maxLength=sqrt((w*w) + (h*h));
    for(int i=0; i<max; i++)
    {
        cin>>temp;
        if(temp<=maxLength)
        {
            cout << "DA" << endl;
        }
        else
        {
            cout << "NE" << endl;
        }
    }
    
    return 0;
}
