//
//  fizzbuzz.cpp
//  fizzbuzz
//
//  Created by Ryan Fleming on 2/22/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int x, y, n;
    std::cin>>x;
    std::cin>>y;
    std::cin>>n;
    for(int i=1; i<=n;i++)
    {
        if(i%x==0)
        {
            std::cout << "Fizz";
        }
        if(i%y==0)
        {
            std::cout << "Buzz";
        }
        if(i%x!=0&&i%y!=0)
        {
            std::cout<<i;
        }
        std::cout<<std::endl;
    }
    
    return 0;
}
