//
//  timeloop.cpp
//  timeloop
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

using namespace std;

int main(int argc, const char * argv[]) {
    int lim;
    cin>>lim;
    for(int i=0; i<lim; i++)
    {
        cout << i+1 << " Abracadabra" << endl;
    }
    return 0;
}
