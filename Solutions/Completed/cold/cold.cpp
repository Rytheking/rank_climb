//
//  cold.cpp
//  cold
//
//  Created by Ryan Fleming on 2/24/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int runs, temp, days=0;
    std::cin>>runs;
    for(int i=0;i<runs;i++)
    {
        std::cin>>temp;
        if(temp<0)
        {
            days++;
        }
    }
    std::cout << days;
    return 0;
}
