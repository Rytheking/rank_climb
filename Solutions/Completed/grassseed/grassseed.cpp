//
//  grassseed.cpp
//  grassseed
//
//  Created by Ryan Fleming on 2/21/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    double cost, length, width, sumArea;
    int lawns;
    sumArea=0;
    std::cin>>cost;
    std::cin>>lawns;
    for(int i=0; i<lawns;i++)
    {
        std::cin>>length;
        std::cin>>width;
        sumArea+=(length*width);
    }
    printf("%.7f", cost*sumArea);
    
    return 0;
}
