//
//  hissingmicrophone.cpp
//  hissingmicrophone
//
//  Created by Ryan Fleming on 2/21/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    std::string a;
    std::cin>>a;
    int b =0;
    for(int i=0;i<a.length()-1;i++)
    {
        if(a.at(i)=='s'&&a.at(i+1)=='s')
        {
            b=1;
        }
    }
    if(b==1)
    {
        std::cout << "hiss" << std::endl;
    }
    else
    {
        std::cout << "no hiss" << std::endl;
    }
    
    return 0;
}
