//
//  heartrate.cpp
//  heartrate
//
//  Created by Ryan Fleming on 2/21/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int runs;
    double beats, time, bpm, minABPM, maxABPM;
    std::cin>>runs;
    for(int i=0; i<runs; i++)
    {
        std::cin>>beats;
        std::cin>>time;
        bpm=(60*beats)/time;
        minABPM=bpm-(bpm/beats);
        maxABPM=bpm+(bpm/beats);
        std::cout << minABPM << " " << bpm << " " << maxABPM << std::endl;
    }
    
    return 0;
}
