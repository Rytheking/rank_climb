//
//  ladder.cpp
//  ladder
//
//  Created by Ryan Fleming on 2/21/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

#include <math.h>

int main(int argc, const char * argv[]) {
    int c;
    double b,B,C;
    std::cin>>b;
    std::cin>>B;
    double pi=atan(1)*4;
    C=B*(pi/180);
    c=ceil(b/sin(C));
    
    std::cout << c;
    return 0;
}
