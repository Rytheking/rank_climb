//
//  cetvrta.cpp
//  cetvrta
//
//  Created by Ryan Fleming on 2/24/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int X[3], Y[3];
    for(int i=0; i<3; i++)
    {
        std::cin>>X[i];
        std::cin>>Y[i];
    }
    if(X[0]==X[1])
    {
        std::cout << X[2] << " ";
    }
    else if(X[1]==X[2])
    {
        std::cout << X[0]<< " ";
    }
    else
    {
        std::cout << X[1]<< " ";
    }
    if(Y[0]==Y[1])
    {
        std::cout << Y[2]<< " ";
    }
    else if(Y[1]==Y[2])
    {
        std::cout << Y[0]<< " ";
    }
    else
    {
        std::cout << Y[1]<< " ";
    }
    
    return 0;
}
