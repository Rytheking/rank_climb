//
//  conundrum.cpp
//  conundrum
//
//  Created by Ryan Fleming on 2/24/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int days=0;
    std::string input;
    std::cin>>input;
    int pos=0;
    for(int i=0;i<input.length();i++)
    {
        pos=(i+1)%3;
        if(pos==1&&(input.at(i)!='P'))
        {
            
            days++;
        }
        if(pos==2&&(input.at(i)!='E'))
        {
            
            days++;
        }
        if(pos==0&&(input.at(i)!='R'))
        {
            days++;
        }
    }
    std::cout<<days<<std::endl;
    return 0;
}
