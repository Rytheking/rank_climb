//
//  trik.cpp
//  Trik
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <cassert>

using namespace std;
int setMove(char a)
{
    int move=0;
    if(a=='A')
    {
        move=1;
    }
    if(a=='B')
    {
        move=2;
    }
    if(a=='C')
    {
        move=3;
    }
    return move;
}
int setPos(int a, int pos)
{
    int p = -1*pos;
    if(a==1&&pos!=3)
    {
        return(p+3);
    }
    
    if(a==2&&pos!=1)
    {
        
        return(p+5);
    }
    
    if(a==3&&pos!=2)
    {
        return(p+4);
    }
    return pos;
   
}
int trik(string a)
{
    string moves=a;
    int pos=1;
    int move;
    cout<<endl;
    int k;
    for(int i=0; i<moves.length(); i++)
    {
        k=i;
        move = setMove(moves[i]);
        pos = setPos(move,pos);
    }
    
    return pos;
}
int main(int argc, const char * argv[]) {
    string a;
    cin>>a;
    cout << trik(a) << endl;
    return 0;
}
