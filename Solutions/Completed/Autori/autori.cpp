//
//  autori.cpp
//  Autori
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <cassert>

using namespace std;




int main(int argc, const char * argv[])
{
    string entry,used,output;
    long pos;
    cin>>entry;
    used=entry;
    while(used.length()>0)
    {
        output+=used.substr(0,1);
        pos=used.find("-");
        if(pos==-1)
        {
            break;
        }
        used=used.substr(pos+1, used.length());
    }
    cout << output << endl;
}
