//
//  lastfactorialdigit.cpp
//  lastfactorialdigit
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    
    int runs, temp;
    std::cin>>runs;
    for(int i=0; i<runs;i++)
    {
        std::cin>>temp;
        if(temp<5)
        {
            switch (temp)
            {
                case 1:
                    std::cout<<1<<std::endl;
                    break;
                case 2:
                    std::cout<<2<<std::endl;
                    break;
                case 3:
                    std::cout<<6<<std::endl;
                    break;
                case 4:
                    std::cout<<4<<std::endl;
                    break;
            }
        }
        else
        {
            std::cout<<0<<std::endl;
        }
    }
    
    return 0;
}
