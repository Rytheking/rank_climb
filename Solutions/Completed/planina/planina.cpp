//
//  planina.cpp
//  planina
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    int runs,points,sum;
    sum=0;
    std::cin>>runs;
    points=2;
    for(int i=0;i<runs;i++)
    {
        points=(points*2)-1;
    }
    sum=points*points;
    std::cout << sum;
    return 0;
}
