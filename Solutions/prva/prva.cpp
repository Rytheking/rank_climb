//
//  prva.cpp
//  prva
//
//  Created by Ryan Fleming on 2/20/19.
//  Copyright © 2019 Ryan Fleming. All rights reserved.
//

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
int main(int argc, const char * argv[]) {
    //declare variables to determine size of crossword, a placeholder string, a vector to store all the words on the crossword
    int rows, columns;
    string temp;
    vector <string> allWords, actualWords;
    //take user input where (2<=rows,columns<=20)
    cin>>rows;
    cin>>columns;
    //take more user input of the crossword's contents (all lowercase letters or '#' symbol to indicate a blackspace on the crossword
    for(int i=0; i<rows+columns;i++)
    {
        //while the input is still the different lines of the crossword puzzle put them at the back of the vector
        if(i<rows)
        {
            cin>>temp;
            allWords.push_back(temp);
        }
        //after all rows have been recieved find the vertical words by taking the 'c' character from each row
        else
        {
                temp="";
                for(int r=0; r<rows; r++)
                {
                    temp+=allWords[r].substr(i-rows,1);
                }
                allWords.push_back(temp);
        }
    }
    //declare iterating variable and a placeholder for positions of black spaces
    int i=0;
    long catch1=0,end, start=0;
    //Because I'm adding to the back of the vector we will keep clearing newly created words with the while loop
    while(i!=allWords.size())
    {
        //find any instances of black space
        catch1=allWords[i].find('#');
        while(catch1!=std::string::npos)
        {
            end=catch1;
            if(allWords[i].substr(start,end).length()>1)
            {
                cout<<allWords[i].substr(start,end)<<endl;
                actualWords.push_back(allWords[i].substr(start,end));
            }
            start=end+1;
            catch1=allWords[i].substr(start,end).find('#');
        }
        end=int (allWords[i].length());
        if(allWords[i].substr(start,end).length()>1)
        {
            actualWords.push_back(allWords[i].substr(start,end));
        }
        i++;
    }
    sort(actualWords.begin(), actualWords.end());
    //output
    cout<<actualWords.front()<<endl;
    return 0;
}
